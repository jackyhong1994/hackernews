import React, { useCallback, useMemo } from 'react'
import axios from 'axios'

axios.defaults.baseURL = 'https://hacker-news.firebaseio.com'

const useAxios = () => {
  const getTopStories = useCallback(async () => {
    try {
      const response = await axios({
        method: 'GET',
        url: '/v0/topstories.json'
      })
      return {
        data: response.data,
        status: response.status,
        loading: false
      }
    } catch (error) {
      console.log('getTopStories response error', error)
    }
  }, [])

  const getStoryItem = useCallback(async ({ item, signal }) => {
    try {
      const response = await axios({
        method: 'GET',
        url: `/v0/item/${item}.json`,
        signal: signal
      })
      return {
        data: response.data,
        status: response.status,
        loading: false
      }
    } catch (error) {
      console.log('getStoryItem response error', error)
    }
  }, [])

  return {
    getTopStories,
    getStoryItem
  }
}

export default useAxios
