import { pluralize, getHostName, getDaysDiff } from '../helper'

describe('helper', () => {
  describe('pluralize function', () => {
    test('count > 1 should show plural word', () => {
      const actual = pluralize({
        count: 10,
        pluralWord: 'Tests',
        singularWord: 'Test'
      })
      const expected = 'Tests'

      expect(actual).toMatch(expected)
    })

    test('count = 1 should show singular word', () => {
      const actual = pluralize({
        count: 1,
        pluralWord: 'Tests',
        singularWord: 'Test'
      })
      const expected = 'Test'

      expect(actual).toMatch(expected)
    })

    test('count = 0 should show singular word', () => {
      const actual = pluralize({
        count: 1,
        pluralWord: 'Tests',
        singularWord: 'Test'
      })
      const expected = 'Test'

      expect(actual).toMatch(expected)
    })

    test('count < 0 should show singular word', () => {
      const actual = pluralize({
        count: -1,
        pluralWord: 'Tests',
        singularWord: 'Test'
      })
      const expected = 'Test'

      expect(actual).toMatch(expected)
    })
  })

  describe('getHostName function', () => {
    test('should get the correct hostname with http:// prefix', () => {
      const url = 'http://www.google.com'
      const actual = getHostName({ url: url })
      const expected = 'google.com'

      expect(actual).toMatch(expected)
    })

    test('should get the correct hostname with https:// prefix', () => {
      const url = 'https://www.google.com'
      const actual = getHostName({ url: url })
      const expected = 'google.com'

      expect(actual).toMatch(expected)
    })

    test('should return empty string for invalid url', () => {
      const url = 'https://'
      const actual = getHostName({ url: url })
      const expected = ''

      expect(actual).toMatch(expected)
    })
  })
})
