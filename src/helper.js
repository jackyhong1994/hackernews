import { formatDistance } from 'date-fns'
export const pluralize = ({ count, pluralWord, singularWord }) =>
  count > 1 ? pluralWord : singularWord

export const getDaysDiff = ({ unixTime }) => {
  return formatDistance(new Date(unixTime * 1000), new Date(), {
    addSuffix: true
  })
}

export const getHostName = ({ url }) => {
  if (url?.length > 0 && isValidUrl(url)) {
    return new URL(url).hostname
  } else {
    return ''
  }
}

const isValidUrl = url => {
  try {
    new URL(url)
  } catch (e) {
    console.error(e)
    return false
  }
  return true
}
