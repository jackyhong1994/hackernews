import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import TopStories from '../modules/TopStories/TopStories'
import StoryPage from '../modules/StoryPage/StoryPage'

const { Navigator, Screen } = createNativeStackNavigator()

const MainRoute = () => {
  return (
    <Navigator initialRouteName="StoryList">
      <Screen
        name="StoryList"
        component={TopStories}
        options={{
          headerTitle: 'Hacker News - Top Stories',
          headerStyle: { backgroundColor: '#ff7b00' }
        }}
      />
      <Screen
        name="StoryPage"
        component={StoryPage}
        options={{
          headerTitle: 'Comment List',
          headerStyle: { backgroundColor: '#ff7b00' }
        }}
      />
    </Navigator>
  )
}

export default MainRoute
