import { useEffect, useState, useCallback, useRef, useMemo } from 'react'
import useAxios from '../../api/useAxios'

const useTopStories = () => {
  const { getTopStories, getStoryItem } = useAxios()
  const [chunk, setChunk] = useState([])
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState(null)
  const [refreshing, setRefreshing] = useState(false)
  const currentPage = useRef(1)
  const totalPage = useRef(0)
  const pageSize = useRef(10)
  const storyIds = useRef([])

  const controller = useRef(new AbortController())

  const isResultEnd = currentPage.current >= totalPage.current

  const retrieveTopStoriesFromChunk = useCallback(
    ({ page, data }) => {
      try {
        const start = (page - 1) * pageSize.current
        const end = page * pageSize.current

        data.slice(start, end).forEach(async item => {
          const story = await getStoryItem({
            item,
            signal: controller.current.signal
          })
          if (story?.data) {
            setChunk(prevState => [...prevState, story.data])
          }
          return item
        })
      } catch (error) {
        console.error('retrieveTopStoriesFromChunk error', error)
      }
    },
    [getStoryItem]
  )

  const retrieveTopStoriesIds = useCallback(async () => {
    try {
      const { data = [], error } = await getTopStories()
      totalPage.current = Math.ceil(data.length / pageSize.current)
      storyIds.current = data
      retrieveTopStoriesFromChunk({ page: currentPage.current, data: data })
      setError(error)
    } catch (error) {
      console.error('error', error)
    }
  }, [getTopStories, retrieveTopStoriesFromChunk])

  useEffect(() => {
    if (chunk.length >= currentPage.current * pageSize.current) {
      setLoading(false)
    }
    if (chunk.length > 0 && refreshing) {
      setRefreshing(false)
    }
  }, [chunk, refreshing])

  useEffect(() => {
    setLoading(true)
    retrieveTopStoriesIds()
  }, [retrieveTopStoriesIds])

  const onRefresh = () => {
    controller.current.abort()
    delete controller.current
    setRefreshing(true)
    setLoading(true)
    setChunk([])
    currentPage.current = 1
    controller.current = new AbortController()
    retrieveTopStoriesIds()
  }

  const appendTopStories = useCallback(() => {
    if (!isResultEnd && !loading) {
      setLoading(true)
      currentPage.current += 1
      retrieveTopStoriesFromChunk({
        page: currentPage.current,
        data: storyIds.current
      })
    }
  }, [isResultEnd, loading, retrieveTopStoriesFromChunk])

  return {
    data: chunk,
    loading,
    error,
    refreshing,
    onRefresh,
    appendTopStories
  }
}

export default useTopStories
