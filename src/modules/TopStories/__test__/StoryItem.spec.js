import React from 'react'
import { render } from '@testing-library/react-native'
import StoryItem from '../components/StoryItem'
import { describe } from 'jest-circus'

describe('StoryItem Component', () => {
  describe('with score and descendants more than 1', () => {
    test('should display plural words', () => {
      const component = render(
        <StoryItem
          title={'Test StoryItem'}
          url={'www.google.com'}
          score={100}
          by={'Jackie'}
          descendants={10}
          time={new Date().getTime() / 1000}
          index={1}
        />
      )
      expect(component.getByTestId('subtitleText').props.children).toEqual(
        '100 points by Jackie less than a minute ago | 10 comments'
      )
    })
  })

  describe('with score and descendants less than or equal to 1', () => {
    test('should diplay story item with correct words', () => {
      const component = render(
        <StoryItem
          title={'Test StoryItem'}
          url={'www.google.com'}
          score={1}
          by={'Jackie'}
          descendants={1}
          time={new Date().getTime() / 1000}
          index={1}
        />
      )
      expect(component.getByTestId('subtitleText').props.children).toEqual(
        '1 point by Jackie less than a minute ago | 1 comment'
      )
    })
  })
})
