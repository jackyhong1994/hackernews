import React from 'react'
import { Text, View, StyleSheet } from 'react-native'
import { pluralize, getDaysDiff, getHostName } from '../../../helper'
import styled from 'styled-components/native'

const SubtitleStyled = styled.Text`
  font-size: 13px;
  color: #00000055;
`

const StoryItem = ({
  title,
  url: propsUrl,
  score,
  by,
  time,
  descendants,
  index
}) => {
  let url = getHostName({ url: propsUrl })
  return (
    <View style={{ flex: 1, flexDirection: 'row' }}>
      <Text style={{ color: '#00000066' }}>{index + 1}.</Text>
      <Text style={{ color: '#00000066' }}>^ </Text>
      <View style={{ flex: 1 }}>
        <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
          <Text>{title} </Text>
          <Text
            style={{
              color: '#00000044',
              fontSize: 13
            }}
          >
            {url.length > 0 ? `(${url})` : ''}
          </Text>
        </View>
        <View style={[styles.subtitle, { flexDirection: 'row' }]}>
          <SubtitleStyled testID={'subtitleText'}>
            {score +
              ' ' +
              pluralize({
                count: score,
                pluralWord: 'points',
                singularWord: 'point'
              }) +
              ' by ' +
              by +
              ' ' +
              getDaysDiff({ unixTime: time }) +
              ' | ' +
              (descendants ?? 0) +
              ' ' +
              pluralize({
                count: descendants,
                pluralWord: 'comments',
                singularWord: 'comment'
              })}
          </SubtitleStyled>
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  subtitle: {
    fontSize: 13,
    color: '#00000033'
  }
})

export default React.memo(StoryItem)
