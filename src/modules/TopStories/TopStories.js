import React, { useCallback } from 'react'
import { FlatList, Pressable, ActivityIndicator } from 'react-native'
import useTopStories from './useTopStories'
import { useNavigation } from '@react-navigation/native'
import StoryItem from './components/StoryItem'

const TopStories = () => {
  const { data, refreshing, onRefresh, appendTopStories } = useTopStories()
  const navigation = useNavigation()

  const onPressItem = useCallback(
    ({ item }) => {
      navigation.navigate('StoryPage', { item: item.kids })
    },
    [navigation]
  )

  const renderStoryItem = ({ item, index }) => (
    <Pressable
      onPress={() => onPressItem({ item })}
      style={{ paddingVertical: 10 }}
    >
      <StoryItem
        title={item.title}
        url={item.url}
        score={item.score}
        by={item.by}
        descendants={item.descendants}
        time={item.time}
        index={index}
      />
    </Pressable>
  )

  const renderEmptyComponent = () => {
    return <ActivityIndicator style={{ padding: 10 }} size={'small'} />
  }

  return (
    <FlatList
      style={{ flex: 1 }}
      contentContainerStyle={{ padding: 10 }}
      data={data}
      refreshing={refreshing}
      onRefresh={onRefresh}
      ListEmptyComponent={renderEmptyComponent}
      keyExtractor={item => item.id}
      renderItem={renderStoryItem}
      removeClippedSubviews
      onEndReached={appendTopStories}
      useNativeDriver
    />
  )
}

export default React.memo(TopStories)
