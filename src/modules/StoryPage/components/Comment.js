import React, { useMemo } from 'react'
import { View, Text, useWindowDimensions } from 'react-native'
import { getDaysDiff } from '../../../helper'
import RenderHtml from 'react-native-render-html'
import styled from 'styled-components/native'

const SubtitleStyled = styled.Text`
  font-size: 13px;
  color: #00000055;
`

const Comment = ({ text, author: propsAuthor, date }) => {
  const { width } = useWindowDimensions()
  const author = useMemo(
    () => (propsAuthor?.length > 0 ? propsAuthor : 'Anonymous'),
    [propsAuthor]
  )

  return (
    <View style={{ padding: 10 }}>
      <View style={{ flexDirection: 'row', marginLeft: -10 }}>
        <Text style={{ color: '#00000066' }}>^ </Text>
        <SubtitleStyled testID={'authorWithDateText'}>
          {author + ' ' + getDaysDiff({ unixTime: date })}
        </SubtitleStyled>
      </View>
      <RenderHtml source={{ html: text ?? '' }} contentWidth={width} />
    </View>
  )
}

export default React.memo(Comment)
