import React from 'react'
import { FlatList, ActivityIndicator, View, Text } from 'react-native'
import { useRoute } from '@react-navigation/native'
import useStoryPage from './useStoryPage'
import Comment from './components/Comment'

const StoryPage = ({ kids }) => {
  const { params } = useRoute()
  const { item } = params
  const { data, loading } = useStoryPage({
    items: kids ?? item ?? []
  })

  const renderStoryItem = ({ item }) => (
    <>
      <Comment text={item.text} author={item.by} date={item.time} />
      {item?.kids?.length > 0 && <StoryPage kids={item.kids} />}
    </>
  )

  const renderEmptyComponent = () => {
    return <ActivityIndicator style={{ padding: 10 }} size={'small'} />
  }

  if (loading || data.length > 0) {
    return (
      <FlatList
        style={{ paddingLeft: 10 }}
        data={data}
        keyExtractor={item => item.id}
        renderItem={renderStoryItem}
        ListEmptyComponent={renderEmptyComponent}
        removeClippedSubviews
        useNativeDriver
      />
    )
  } else {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          padding: 10
        }}
      >
        <Text style={{ fontWeight: 'bold' }}>No Comment</Text>
      </View>
    )
  }
}

export default React.memo(StoryPage)
