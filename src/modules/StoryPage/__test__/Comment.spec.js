import React from 'react'
import { render } from '@testing-library/react-native'
import Comment from '../components/Comment'

describe('Comment Component', () => {
  test('should diplay author, date and text.', () => {
    const component = render(
      <Comment
        author={'Jackie'}
        date={new Date().getTime() / 1000}
        text={'Hello'}
      />
    )
    expect(component.getByTestId('authorWithDateText').props.children).toEqual(
      'Jackie less than a minute ago'
    )
    expect(component.getByText('Hello')).toBeTruthy()
  })

  test('should diplay "Anonymous" author and date when author is empty.', () => {
    const component = render(
      <Comment author={''} date={new Date().getTime() / 1000} text={'Hello'} />
    )
    expect(component.getByTestId('authorWithDateText').props.children).toEqual(
      'Anonymous less than a minute ago'
    )
  })
})
