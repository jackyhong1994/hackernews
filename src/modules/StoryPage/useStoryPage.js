import { useEffect, useState, useCallback, useRef } from 'react'
import useAxios from '../../api/useAxios'

const useStoryPage = ({ items }) => {
  const { getStoryItem } = useAxios()

  const [data, setData] = useState([])
  const [loading, setLoading] = useState(true)
  const [error, setError] = useState(null)
  const controller = useRef(new AbortController())
  const isUnmounted = useRef(false)

  const retrieveStoryFromApi = useCallback(async () => {
    try {
      await Promise.all(
        items.map(async item => {
          const story = await getStoryItem({
            item,
            signal: controller.current.signal
          })
          if (story?.data) {
            setData(prevState => [...prevState, story.data])
          }
          return item
        })
      )
    } catch (error) {
      console.error('error', error)
    }
  }, [items, getStoryItem])

  const retrieveStory = useCallback(async () => {
    try {
      await retrieveStoryFromApi()
      if (!isUnmounted.current) {
        setLoading(false)
      }
    } catch (error) {
      console.error('error', error)
    }
  }, [retrieveStoryFromApi])

  useEffect(() => {
    const abortController = controller.current
    retrieveStory()
    return () => {
      isUnmounted.current = true
      abortController.abort()
    }
  }, [retrieveStory])

  return {
    data,
    loading,
    error,
    isUnmounted
  }
}

export default useStoryPage
